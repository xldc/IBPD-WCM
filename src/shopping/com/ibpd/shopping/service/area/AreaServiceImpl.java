package com.ibpd.shopping.service.area;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.exception.IbpdException;
import com.ibpd.shopping.entity.AreaEntity;
@Service("areaService")
public class AreaServiceImpl extends BaseServiceImpl<AreaEntity> implements IAreaService {
	public AreaServiceImpl(){
		super();
		this.tableName="AreaEntity";
		this.currentClass=AreaEntity.class;
		this.initOK();
	}

	public List<AreaEntity> getAreaListByParentCode(String pCode) {
		return getList("from " + this.getTableName() + " where pcode='"+pCode+"'",null);
	}

	public String getAreaNameByCode(String areaCode) throws IbpdException {
		if(StringUtils.isBlank(areaCode))
			return "";
		List<AreaEntity> areaList=getList("from " + this.getTableName() + " where code='"+areaCode+"'",null);
		if(areaList!=null && areaList.size()==1){
			return areaList.get(0).getName();
		}else{
			if(areaList.size()==0){
				return "";
			}else{
				throw new IbpdException("存在多个code为["+areaCode+"]的区域实体");				
			}
		}
	}
	public AreaEntity getAreaByCode(String areaCode) {
		if(StringUtils.isBlank(areaCode))
			return null;
		List<AreaEntity> areaList=getList("from " + this.getTableName() + " where code='"+areaCode+"'",null);
		if(areaList!=null && areaList.size()>=1){
			return areaList.get(0);
		}else{
			return null;				
		}
	}
 
	public AreaEntity getAreaByName(String areaName) throws IbpdException {
		if(StringUtils.isBlank(areaName))
			return null;
		List<AreaEntity> areaList=getList("from " + this.getTableName() + " where name='"+areaName+"'",null);
		if(areaList!=null && areaList.size()==1){
			return areaList.get(0);
		}else{
			if(areaList.size()==0){
				return null;
			}else{
				throw new IbpdException("存在多个code为["+areaName+"]的区域实体");				
			}
		}
	}


}
