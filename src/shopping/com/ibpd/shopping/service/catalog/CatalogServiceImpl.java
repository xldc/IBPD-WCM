package com.ibpd.shopping.service.catalog;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.CatalogEntity;
@Service("catalogService")
public class CatalogServiceImpl extends BaseServiceImpl<CatalogEntity> implements ICatalogService {
	public CatalogServiceImpl(){
		super();
		this.tableName="CatalogEntity";
		this.currentClass=CatalogEntity.class;
		this.initOK();
	}

	public List<CatalogEntity> getList(Long parentId) {
		return getList("from "+getTableName()+" where pid="+parentId,null,"order asc",1000,1);
	}

	public List<CatalogEntity> getList(String type) {
		return getList("from "+getTableName()+" where type='"+type+"'",null,"order asc",1000,1);
	}
}
