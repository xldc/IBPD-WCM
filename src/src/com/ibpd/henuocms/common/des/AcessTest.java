package com.ibpd.henuocms.common.des;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class AcessTest {
  
 /**  
     * 根据Key 读取Value  
     *   
     * @param key  
     * @return  
 */  
  public static String readValueByKey(String filePath,String key) {   
         Properties props = new Properties();   
         try {   
             InputStream in = new BufferedInputStream(new FileInputStream(filePath));   
             props.load(in);   
             in.close();   
             String value = props.getProperty(key);   
             return value;   
         } catch (Exception e) {   
             e.printStackTrace();   
             return null;   
         }finally{
        	 
         }
     } 
}