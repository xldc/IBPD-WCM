package com.ibpd.henuocms.service.contentAttr;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.HqlParameter.DataType_Enum;
import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;
@Transactional
@Service("contentAttrService")
public class ContentAttrServiceImpl extends BaseServiceImpl<ContentAttrEntity> implements IContentAttrService {
	public ContentAttrServiceImpl(){
		super();
		this.tableName="ContentAttrEntity";
		this.currentClass=ContentAttrEntity.class;
		this.initOK();
	} 
	/**
	 * 根据内容ID检索出 内容对应的内容属性实体对象
	 * @param contentId
	 * @return
	 */
	public ContentAttrEntity getContentAttr(Long contentId) {
		if(contentId!=null && contentId!=-1){
			List<HqlParameter> hqlList=new ArrayList<HqlParameter>();
			hqlList.add(new HqlParameter("id",contentId,DataType_Enum.Long));
			List<ContentAttrEntity> contentAttrList=this.getList("from "+this.getTableName()+" where contentId=:id", hqlList);
			if(contentAttrList==null)
				return null;
			if(contentAttrList.size()>0){
				return contentAttrList.get(0);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	public String getCopyToContentIds(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return "";
		}else{
			return ca.getCopyToContentIds()==null?"":ca.getCopyToContentIds();
		}
	}
	public String getColor(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return "";
		}else{
			return ca.getColor()==null?"":ca.getColor();
		}
	}
	public String getCopyTo(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return "";
		}else{
			return ca.getCopyTo()==null?"":ca.getCopyTo();
		}
	}
	public Boolean getEm(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return false;
		}else{
			return ca.getEm()==null?false:ca.getEm();
		}
	}
	public String getLinkTo(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return "";
		}else{
			return ca.getLinkTo()==null?"":ca.getLinkTo();
		}
	}
	public String getSize(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return "";
		}else{
			return ca.getSize()==null?"":ca.getSize();
		}
	}
	public Boolean getStrong(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return false;
		}else{
			return ca.getStrong()==null?false:ca.getStrong();
		}
	}
	public String getTitleFormat(Long contentId) {
		Boolean strong=getStrong(contentId);
		Boolean em=getEm(contentId);
		Boolean u=getU(contentId);
		String size=getSize(contentId);
		String color=getColor(contentId);
		//titleFormatParams=color:#FF0000;strong:;em:;u:;size: copyTo= linkTo=
//		return "{'strong':'"+((strong)?"1":"")+"','em':'"+((em)?"1":"")+"','u':'"+((u)?"1":"")+"','size':'"+size+"','color':'"+color+"'}";
		return "strong:"+((strong)?"1":"")+";em:"+((em)?"1":"")+";u:"+((u)?"1":"")+";size:"+size+";color:"+color+"";
	}
	public Boolean getU(Long contentId) {
		ContentAttrEntity ca=getContentAttr(contentId);
		if(ca==null){
			return false;
		}else{
			return ca.getU()==null?false:ca.getU();
		}
	}
	public ContentAttrEntity getContentByCopyIds(Long contentId) {
		List<ContentAttrEntity> l=getList("from "+getTableName()+" where copyToContentIds like '%,"+contentId+",%'",null);
		if(l==null)
			return null;
		if(l.size()>0)
			return l.get(0);
		if(l.size()==0)
			return null;
		return null;
	}
	public List<ContentAttrEntity> getAttrListByLinkToNodeId(Long nodeId) {
		if(nodeId==null || nodeId<=0L)
			return null;
		List<ContentAttrEntity> l=getList("from "+getTableName()+" where linkTo like '%:"+nodeId+":%'",null);
		if(l==null)
			return null;
		if(l.size()>0)
			return l;
		if(l.size()==0)
			return null;
		return null;
	}
}