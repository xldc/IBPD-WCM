package com.ibpd.henuocms.exception;
/**
 * 自定义异常类
 * @author mg by qq:349070443
 *
 */
public class IbpdException extends Exception {

	public IbpdException(String string) {
		super(string);
	}
 
}
