package com.ibpd.henuocms.web.controller.manage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.CompressPicUtil;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.PDF2SWFUtil;

@Controller
public class Upload extends BaseController {
	@RequestMapping("manage/upload/index.do")
	public String index(Long modelId) throws IOException{
		return "manage/upload/index";
	}	
	@RequestMapping("manage/upload/doUpload.do")
	public void doUpload(String s_type,String s_dir,Integer s_index,HttpServletRequest request,HttpServletResponse response) throws IOException{
		IbpdLogger.getLogger(this.getClass()).info(s_type);
		IbpdLogger.getLogger(this.getClass()).info(s_dir);
		IbpdLogger.getLogger(this.getClass()).info(s_index);
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// 设置内存缓冲区，超过后写入临时文件
		factory.setSizeThreshold(10240000);
		// 设置临时文件存储位置
		String basePath = request.getRealPath("/")+"publish"+File.separatorChar+s_dir;
		String pagePath = basePath+File.separatorChar+"files"+File.separatorChar+"page";
		String mobilePath = basePath+File.separatorChar+"files"+File.separatorChar+"mobile";
		String thumbPath = request.getRealPath("/")+"publish"+File.separatorChar+s_dir+File.separatorChar+"files"+File.separatorChar+"thumb";//125*180
//		String base = request.getRealPath("/")+"publish"+File.separatorChar+s_dir+File.separatorChar+"files"+"shot.jpg";//125*180
		File fbasePath = new File(basePath);
		if(!fbasePath.exists())
			fbasePath.mkdirs();
		File nPath=new File(basePath+File.separatorChar+"files");
		if(!nPath.exists())
			nPath.mkdir();
		File fpagePath = new File(pagePath);
		if(!fpagePath.exists())
			fpagePath.mkdirs();
		File fmobilePath = new File(mobilePath);
		if(!fmobilePath.exists())
			fmobilePath.mkdirs();
		File fthumbPath = new File(thumbPath);
		if(!fthumbPath.exists())
			fthumbPath.mkdirs();

		if(s_type.toLowerCase().trim().equals("periodical")){
			IbpdLogger.getLogger(this.getClass()).info(basePath);
			File file = new File(basePath);
			if(!file.exists())
				file.mkdirs();
			factory.setRepository(file);
			ServletFileUpload upload = new ServletFileUpload(factory);
			// 设置单个文件的最大上传值
			upload.setFileSizeMax(22097512);
			// 设置整个request的最大值
			upload.setSizeMax(33097512);
			upload.setHeaderEncoding("UTF-8");
			
			try {
				List<?> items = upload.parseRequest(request);
				FileItem item = null;
				String fileName = null;
//				if(items.size()>0){
//					item = (FileItem) items.get(0);
//					fileName = mobilePath + File.separator + s_index+".jpg";
//					// 保存移动版的图片文件
//					if (!item.isFormField() && item.getName().length() > 0) {
//						item.write(new File(fileName));
//					}
//					//保存完成后，转换图片文件为swf文件
//					PDF2SWFUtil.jpg2swf(fileName, pagePath + File.separator +s_index+".swf");
//				}
				for (int i = 0 ;i < items.size(); i++){
					item = (FileItem) items.get(i);
					fileName = mobilePath + File.separator + s_index+".jpg";
					// 保存移动版的图片文件
					if (!item.isFormField() && item.getName().length() > 0) {
						item.write(new File(fileName));
						//保存完成后，转换图片文件为swf文件
						PDF2SWFUtil.jpg2swf(fileName, pagePath + File.separator +s_index+".swf");
						//生成缩略图
		                 CompressPicUtil mypic = new CompressPicUtil(); 
		                 IbpdLogger.getLogger(this.getClass()).info("输入的图片大小：" + mypic.getPicSize(fileName)/1024 + "KB"); 
		                mypic.compressPic(fileName, thumbPath + File.separator + s_index+".jpg", "", "", 125, 180, false); 

					}

				}
			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}	
}
